 



Network Working Group                                        T. Citharel
Internet-Draft                                                 Framasoft
Intended status: Standards Track                       November 30, 2018
Expires: July 3, 2019                                                   



                         The 'group' URI Scheme
                         draft-tcit-group-uri-01

Abstract

   This document defines the 'group' Uniform Resource Identifier (URI)
   scheme as a way to identify a group at a service provider,
   irrespective of the particular protocols that can be used to interact
   with the group.

Status of This Memo

   This Internet-Draft is submitted in full conformance with the
   provisions of BCP 78 and BCP 79.

   Internet-Drafts are working documents of the Internet Engineering
   Task Force (IETF).  Note that other groups may also distribute
   working documents as Internet-Drafts.  The list of current Internet-
   Drafts is at http://datatracker.ietf.org/drafts/current/.

   Internet-Drafts are draft documents valid for a maximum of six months
   and may be updated, replaced, or obsoleted by other documents at any
   time.  It is inappropriate to use Internet-Drafts as reference
   material or to cite them other than as "work in progress."

   This Internet-Draft will expire on July 3, 2019.

Copyright Notice

   Copyright (c) 2018 IETF Trust and the persons identified as the
   document authors.  All rights reserved.

   This document is subject to BCP 78 and the IETF Trust's Legal
   Provisions Relating to IETF Documents
   (http://trustee.ietf.org/license-info) in effect on the date of
   publication of this document.  Please review these documents
   carefully, as they describe your rights and restrictions with respect
   to this document.  Code Components extracted from this document must
   include Simplified BSD License text as described in Section 4.e of
   the Trust Legal Provisions and are provided without warranty as
   described in the Simplified BSD License.

 


Citharel                    Standards Track                     [Page 1]

RFC 7565                 The 'group' URI Scheme            November 2018


Table of Contents

   1. Introduction ....................................................2
   2. Terminology .....................................................2
   3. Rationale .......................................................2
   4. Definition ......................................................3
   5. Security Considerations .........................................4
   6. Internationalization Considerations .............................5
   7. IANA Considerations .............................................5
   8. References ......................................................6
      8.1. Normative References .......................................6
      8.2. Informative References .....................................7
   Acknowledgements ...................................................8
   Author's Address ...................................................8

1.  Introduction

   Existing Uniform Resource Identifier (URI) schemes that enable
   interaction with, or that identify resources associated with, a
   user's account at a service provider are tied to particular services
   or application protocols.  Two examples are the 'mailto' scheme
   (which enables interaction with a user's email account) and the
   'http' scheme (which enables retrieval of web files controlled by a
   user or interaction with interfaces providing information about a
   user). The 'acct' scheme was also added with [RFC7565] to identify an
   account at a service provider. However, there exists no URI scheme
   that generically identifies a group at a service provider without
   specifying a particular protocol to use when interacting with the
   group. This specification fills that gap.

2.  Terminology

   The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
   "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and
   "OPTIONAL" in this document are to be interpreted as described in
   [RFC2119].

3.  Rationale

   With the multiplication of decentralized social networks that rely
   heavily on the WebFinger protocol [RFC7033] to discover users easily.
   The accessible form with the 'acct' URI [RFC7565] is used a lot, but
   it has a restriction on being used to identify something that has an
   account (an user, a bot, etc.). Therefore it doesn't allows quering
   information about a group. Even if a WebFinger query with the 'acct'
   URI and group name as username is done, there would be no way to
   identify if the returned information is about a group or an user.

 


Citharel                    Standards Track                     [Page 2]

RFC 7565                 The 'group' URI Scheme            November 2018


   (Note that a group could be called Community.)

4.  Definition

   The syntax of the 'group' URI scheme is defined under Section 7 of
   this document.  Although 'group' URIs take the form "group@host", the
   scheme is designed for the purpose of identification instead of
   interaction (regarding this distinction, see Section 1.2.2 of
   [RFC3986]).  The "Internet resource" identified by an 'group' URI is
   a group hosted at a service provider, where the service provider is
   typically associated with a DNS domain name.  Thus, a particular
   'group' URI is formed by setting the group portion to the group name
   at the service provider and by setting the "host" portion to the DNS
   domain name of the service provider.

   Consider the case of a group with an name of "foobar" on a
   microblogging service "status.example.net".  It is taken as
   convention that the string "foobar@status.example.net" designates
   that group. This is expressed as a URI using the 'group' scheme as
   "group:foobar@status.example.net".

   A common scenario is for a person to join a group associated with a
   specific service provider. For example, a person sees on a public
   website whose domain name is "site.social" a list of groups. If she
   picks the group "discworld", the resulting 'group' URI would be
   "group:discworld@site.social".

   It is not assumed that an entity will necessarily be able to interact
   with a group using any particular application protocol, such as
   email; to enable such interaction, an entity would need to use the
   appropriate URI scheme for such a protocol, such as the 'mailto'
   scheme.  While it might be true that the 'group' URI minus the scheme
   name (e.g., "mygroup@example.com" derived from
   "group:mygroup@example.com") can be reached via email or some other
   application protocol, that fact would be purely contingent and
   dependent upon the deployment practices of the provider.

   Because an 'group' URI enables abstract identification only and not
   interaction, this specification provides no method for dereferencing
   an 'group' URI on its own, e.g., as the value of the 'href' attribute
   of an HTML anchor element.  For example, there is no behavior
   specified in this document for an 'group' URI used as follows:

   <a href='group:girls@example.com'>find out more</a>

   Any protocol that uses 'group' URIs is responsible for specifying how
   an 'group' URI is employed in the context of that protocol (in
   particular, how it is dereferenced or resolved; see [RFC3986]).  As a
 


Citharel                    Standards Track                     [Page 3]

RFC 7565                 The 'group' URI Scheme            November 2018


   concrete example, an "Account Information" application of the
   WebFinger protocol [RFC7033] might take an 'group' URI, resolve the
   host portion to find a WebFinger server, and then pass the 'group'
   URI as a parameter in a WebFinger HTTP request for metadata (i.e.,
   web links [RFC8288]) about the resource.  For example:

   GET /.well-known/webfinger?resource=group%3Agirls%40example.com
   HTTP/1.1

   The service retrieves the metadata associated with the group
   identified by that URI and then provides that metadata to the
   requesting entity in an HTTP response.

   If an application needs to compare two 'group' URIs (e.g., for
   purposes of authentication and authorization), it MUST do so using
   case normalization and percent-encoding normalization as specified in
   Sections 6.2.2.1 and 6.2.2.2 of [RFC3986].

5.  Security Considerations

   Because the 'group' URI scheme does not directly enable interaction
   with a group at a service provider, direct security concerns are
   minimized.

   However, a 'group' URI does provide proof of existence of the group;
   this implies that harvesting published 'group' URIs could prove
   useful to spammers and similar attackers -- for example, if they can
   use an 'group' URI to leverage more information about the group
   (e.g., via WebFinger) or if they can interact with protocol- specific
   URIs (such as 'mailto' URIs) whose group@host portion is the same as
   that of the 'group' URI.

   In addition, protocols that make use of 'group' URIs are responsible
   for defining security considerations related to such usage, e.g., the
   risks involved in dereferencing an 'group' URI, the authentication
   and authorization methods that could be used to control access to
   personal data associated with a group account at a service, and
   methods for ensuring the confidentiality of such information.

   The use of percent-encoding allows a wider range of characters in
   group names but introduces some additional risks.  Implementers are
   advised to disallow percent-encoded characters or sequences that
   would (1) result in space, null, control, or other characters that
   are otherwise forbidden, (2) allow unauthorized access to private
   data, or (3) lead to other security vulnerabilities.

6.  Internationalization Considerations

 


Citharel                    Standards Track                     [Page 4]

RFC 7565                 The 'group' URI Scheme            November 2018


   As specified in [RFC3986], the 'group' URI scheme allows any
   character from the Unicode repertoire [Unicode] encoded as UTF-8
   [RFC3629] and then percent-encoded into valid ASCII [RFC20].  Before
   applying any percent-encoding, an application MUST ensure the
   following about the string that is used as input to the URI-
   construction process:

   o  The userpart consists only of Unicode code points that conform to
      the PRECIS IdentifierClass specified in [RFC8264].

   o  The host consists only of Unicode code points that conform to the
      rules specified in [RFC5892].

   o  Internationalized domain name (IDN) labels are encoded as A-labels
      [RFC5890].

7.  IANA Considerations

   In accordance with the guidelines and registration procedures for new
   URI schemes [RFC7595], this section provides the information needed
   to register the 'group' URI scheme.

   URI Scheme Name:  group

   Status:  permanent

   URI Scheme Syntax:  The 'group' URI syntax is defined here in
      Augmented Backus-Naur Form (ABNF) [RFC5234], borrowing the 'host',
      'pct-encoded', 'sub-delims', and 'unreserved' rules from
      [RFC3986]:

      groupURI      = "group" ":" grouppart "@" host
      grouppart     = unreserved / sub-delims
                     0*( unreserved / pct-encoded / sub-delims )

      Note that additional rules regarding the strings that are used as
      input to construction of 'group' URIs further limit the characters
      that can be percent-encoded; see the Encoding Considerations as
      well as Section 6 of this document.


   URI Scheme Semantics:  The 'group' URI scheme identifies groups
      hosted at service providers.  It is used only for identification,
      not interaction.  A protocol that employs the 'group' URI scheme
      is responsible for specifying how an 'group' URI is dereferenced
      in the context of that protocol.  There is no media type
      associated with the 'group' URI scheme.

 


Citharel                    Standards Track                     [Page 5]

RFC 7565                 The 'group' URI Scheme            November 2018


   Encoding Considerations:  See Section 6 of this document.

   Applications/Protocols That Use This URI Scheme Name:  At the time of
      this writing, only the WebFinger protocol uses the 'group' URI
      scheme.  However, use is not restricted to the WebFinger protocol,
      and the scheme might be considered for use in other protocols.

   Interoperability Considerations:  There are no known interoperability
      concerns related to use of the 'group' URI scheme.

   Security Considerations:  See Section 5 of this document.

   Contact:  Peter Saint-Andre, peter@andyet.com

   Author/Change Controller:  This scheme is registered under the IETF
      tree.  As such, the IETF maintains change control.

   References:  None.

8.  References

8.1.  Normative References

   [RFC2119]  Bradner, S., "Key words for use in RFCs to Indicate
              Requirement Levels", BCP 14, RFC 2119,
              DOI 10.17487/RFC2119, March 1997,
              <http://www.rfc-editor.org/info/rfc2119>.

   [RFC3629]  Yergeau, F., "UTF-8, a transformation format of
              ISO 10646", STD 63, RFC 3629, DOI 10.17487/RFC3629,
              November 2003, <http://www.rfc-editor.org/info/rfc3629>.

   [RFC3986]  Berners-Lee, T., Fielding, R., and L. Masinter, "Uniform
              Resource Identifier (URI): Generic Syntax", STD 66,
              RFC 3986, DOI 10.17487/RFC3986, January 2005,
              <http://www.rfc-editor.org/info/rfc3986>.

   [RFC5234]  Crocker, D., Ed., and P. Overell, "Augmented BNF for
              Syntax Specifications: ABNF", STD 68, RFC 5234,
              DOI 10.17487/RFC5234, January 2008,
              <http://www.rfc-editor.org/info/rfc5234>.

   [RFC5890]  Klensin, J., "Internationalized Domain Names for
              Applications (IDNA): Definitions and Document Framework",
              RFC 5890, DOI 10.17487/RFC5890, August 2010,
              <http://www.rfc-editor.org/info/rfc5890>.

   [RFC5892]  Faltstrom, P., Ed., "The Unicode Code Points and
 


Citharel                    Standards Track                     [Page 6]

RFC 7565                 The 'group' URI Scheme            November 2018


              Internationalized Domain Names for Applications (IDNA)",
              RFC 5892, DOI 10.17487/RFC5892, August 2010,
              <http://www.rfc-editor.org/info/rfc5892>.

   [RFC8264]  Saint-Andre, P. and M. Blanchet, "PRECIS Framework:
              Preparation, Enforcement, and Comparison of
              Internationalized Strings in Application Protocols",
              RFC 8264, DOI 10.17487/RFC8264, October 2017,
              <http://www.rfc-editor.org/info/rfc8264>.

   [Unicode]  The Unicode Consortium, "The Unicode Standard",
              <http://www.unicode.org/versions/latest/>.

8.2.  Informative References

   [RFC20]    Cerf, V., "ASCII format for network interchange", STD 80,
              RFC 20, DOI 10.17487/RFC0020, October 1969,
              <http://www.rfc-editor.org/info/rfc20>.

   [RFC7595]  D. Thaler, Ed., T. Hansen, T. Hardie, "Guidelines and
              Registration Procedures for New URI Schemes", BCP 35,
              RFC 7595, DOI 10.17487/RFC7595, June 2015,
              <http://www.rfc-editor.org/info/rfc7595>.

   [RFC8288]  Nottingham, M., "Web Linking", RFC 8288,
              DOI 10.17487/RFC8288, October 2017,
              <http://www.rfc-editor.org/info/rfc8288>.

   [RFC7565]  P. Saint-Andre, "The 'acct' URI Scheme", RFC 7565, DOI
              10.17487/RFC7565, May 2015,
              <http://www.rfc-editor.org/info/rfc7565>.

   [RFC7033]  Jones, P., Salgueiro, G., Jones, M., and J. Smarr,
              "WebFinger", RFC 7033, DOI 10.17487/RFC7033,
              September 2013, <http://www.rfc-editor.org/info/rfc7033>.













 


Citharel                    Standards Track                     [Page 7]

RFC 7565                 The 'group' URI Scheme            November 2018


Acknowledgements

   Thanks to Chocobozzz, Bat for the exchanges.

Author's Address

   Thomas Citharel

   EMail: tcit@tcit.fr
   URI:   https://tcit.fr









































Citharel                    Standards Track                     [Page 8]
